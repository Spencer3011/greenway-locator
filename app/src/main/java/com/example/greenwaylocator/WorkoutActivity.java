/*  Greenway Locator
 *  Group 11
 *  WorkoutActivity.java
 *  This class is used to allow the user to select a type of workout and pass the information on to the TrackerActivity
 * */
package com.example.greenwaylocator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class WorkoutActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    TextView weight;
    String weightDisplay;
    String activity;
    String userWeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_workout);

        weight = findViewById(R.id.weightVal);
        try {
            weightDisplay = getIntent().getExtras().getString("weight");
            weight.setText("Your weight in KG is " + weightDisplay);
            userWeight = weightDisplay;
        } catch (Exception e) {
            weight.setText("Enter weight on Health Tab!");
            userWeight = null;
        }


        Button walk  = (Button)findViewById(R.id.walk);
        Button run  = (Button)findViewById(R.id.run);
        Button bike  = (Button)findViewById(R.id.bike);

        bottomNavigationView = findViewById(R.id.nav_bar);

        bottomNavigationView.setSelectedItemId(R.id.nav_workouts);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_greenways:
                        startActivity(new Intent(getApplicationContext()
                                ,MainActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_health:
                        startActivity(new Intent(getApplicationContext()
                                ,HealthActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.nav_tracker:
                        startActivity(new Intent(getApplicationContext()
                                ,TrackerActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });

        walk.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                activity = "Walking";
                Intent i = new Intent(WorkoutActivity.this, TrackerActivity.class);
                i.putExtra("activity",activity);
                i.putExtra("weight",userWeight);
                startActivity(i);
                finish();
            }
        });

        run.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                activity = "Running";
                Intent i = new Intent(WorkoutActivity.this, TrackerActivity.class);
                i.putExtra("activity",activity);
                i.putExtra("weight",userWeight);
                startActivity(i);
                finish();
            }
        });

        bike.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                activity = "Biking";
                Intent i = new Intent(WorkoutActivity.this, TrackerActivity.class);
                i.putExtra("activity",activity);
                i.putExtra("weight",userWeight);
                startActivity(i);
                finish();
            }
        });
    }
}