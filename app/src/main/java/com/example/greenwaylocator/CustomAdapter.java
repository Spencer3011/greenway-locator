/*  Greenway Locator
 *  Group 11
 *  CustomAdapter.java
 *  This class is used to display each greenway correctly in the list view located in the MainActivity
 * */
package com.example.greenwaylocator;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class CustomAdapter extends BaseAdapter {
    // Variables Initialized
    Context context;
    List<Greenway> greenways;

    CustomAdapter(Context context, List<Greenway> greenways) {
        this.context = context;
        this.greenways = greenways;
    }

    @Override
    public int getCount() {
        return greenways.size();
    }

    @Override
    public Object getItem(int position) {
        return greenways.get(position);
    }

    @Override
    public long getItemId(int position) {
        return greenways.indexOf(getItem(position));
    }

    private class ViewHolder {
        ImageView preview_pic;
        TextView greenway_name;
        TextView latitude;
        TextView longitude;
    }

    // This method controls what information is displayed in the listview and where its displayed
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder();

            holder.greenway_name = (TextView) convertView.findViewById(R.id.greenway_name);
            holder.preview_pic = (ImageView) convertView.findViewById(R.id.preview_pic);
            holder.latitude = (TextView) convertView.findViewById(R.id.latitude);
            holder.longitude = (TextView) convertView.findViewById(R.id.longitude);

            Greenway row_position = greenways.get(position);

            holder.preview_pic.setImageResource(row_position.getPreview_pic_id());
            holder.greenway_name.setText(row_position.getGreenway_name());
            holder.latitude.setText(row_position.getLatitude());
            holder.longitude.setText(row_position.getLongitude());
        }
        // Returns finished view
        return convertView;
    }
}
