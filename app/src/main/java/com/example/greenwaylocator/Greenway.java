/*  Greenway Locator
 *  Group 11
 *  Greenway.java
 *  This class is used to create each Greenway Object.
 * */
package com.example.greenwaylocator;

public class Greenway {
    // Initialize Variables
    private String greenway_name;
    private int preview_pic_id;
    private String latitude;
    private String longitude;
    private String address;

    public Greenway(String greenway_name, int preview_pic_id, String latitude, String longitude, String address) {
        this.greenway_name = greenway_name;
        this.preview_pic_id = preview_pic_id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }
    // Getter and Setter methods
    public String getGreenway_name() {
        return greenway_name;
    }

    public void setGreenway_name(String greenway_name) {
        this.greenway_name = greenway_name;
    }

    public int getPreview_pic_id() {
        return preview_pic_id;
    }

    public void setPreview_pic_id(int preview_pic_id) {
        this.preview_pic_id = preview_pic_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
